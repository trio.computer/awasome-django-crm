# OTA Insight test for Lead Backend Engineer

This repository contains the code regarding the coding test for the Lead Backend Engineer role at OTA Insight.

## The assignment

### Part 1: Build a mini-version of a CRM system

#### Installation

Install the required dependencies
```bash
pip install Django
pip install djangorestframework
```

Run the migrations in the project root
```bash
python manage.py migrate
```

#### Start from scratch

Create a new super user in the project root
```bash
python manage.py createsuperuser
```

#### Start from existing data

You can also opt to use the database dump included in this project by running this command in your project root
```
python manage.py loaddata db_dump.json
```

You can sign in with the superuser by using login `jens` and password `trio`.


#### Run the project

Start the server by executing the following command in the project root:  
`python manage.py runserver`

To enter the admin, visit `http://127.0.0.1:8000/admin` in your browser and sign in using the credentials of the super user

This application has one GET endpoint to retrieve the unpaid invoices of the customer.
You can see the list by visiting `http://127.0.0.1:8000/api/customer/<customerId>/invoices/unpaid` in your browser, and replacing `<customerId>` with the actual integer id of the customer.

#### Rate limiting

To prevent abuse of the api, there should be rate limiting in place (together with authentication).

There are multiple ways to achieve rate limting:
- Use the throttling functionality of Django Rest Framework: https://www.django-rest-framework.org/api-guide/throttling/
- Use an external package like https://django-ratelimit.readthedocs.io/en/stable/
- Use a tool like Redis for rate limiting: https://developer.redis.com/howtos/ratelimiting/

#### Assumtions

- Customer currency cannot be changed once there is an invoice since that would invalidate the invoice
- Since this is a PoC, I did not think it was needed to add translation to the things I added to the template
- To prevent confusion, I used `Customer` instead of `User` as the model name
- I assumed the invoice numbers keep counting up, without resetting every year
- For legal matters, invoices shouldn't be allowed to edit once they are sent. Since this is a PoC with limited functionality, I assumed this was not the case here.
- I did not include unit tests because
  - There is no real business logic in this application (although this is up for discussion)
  - I am new to Pyton and Django, and I'm not familiar in how to test certain units, so I decided not to include it.
  - Unit tests are a must for a real-life application
- There was no authentication requested in the assignment, so I did not provide one in the API
- Company is an optional field since not every customer is professional

#### Next steps

- Architecture:
  - Structure the code according to the conventions
  - Include a Docker image to run the application in a container
- For the `Customer` model
  - Make `currency` a dropdown with allowed customers
  - Make `country` a dropdown with allowed customers
  - Don't allow changing the currency once at least on invoice is linked
    - Right now, you cannot update it once the customer is saved
    - This is because the currency is on customer level instead of on invoice level. Changing the customer would invalidate all existing invoices
  - For the `Invoice` model
    - Do not allow creating an invoice earlier than the latest invoices

### Part 2: Sales Automation (SA) and Payment System (PS) integration

See [this link](https://monograph.notesnook.com/63e15ba8a0a0a0376ed4d4d6) for the document
