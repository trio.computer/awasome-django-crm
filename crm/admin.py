from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.db.models import Sum
from django.utils.html import format_html
from django.urls import reverse
from datetime import date
from dateutil.relativedelta import relativedelta


from .models import Customer, Invoice

class OverThousandFilter(admin.SimpleListFilter):
    title = _('customer billing amount')
    parameter_name = 'total_invoice_totalAmount'

    def lookups(self, request, model_admin):
        return (
            ('over_1000', _('Billed over 1000')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'over_1000':
           return queryset.annotate(
                total_invoice_totalAmount=Sum('invoice__totalAmount')
            ).filter(total_invoice_totalAmount__gt=1000)
        return queryset

class CustomerAdmin(admin.ModelAdmin):
    change_form_template = 'customer_detail.html'

    list_display = [
        'name',
        'email',
        'companyName',
        'country',
        'invoicingCurrencyCode',
        'view_invoices',
    ]

    list_filter = (OverThousandFilter,)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['invoicingCurrencyCode']
        return self.readonly_fields

    def view_invoices(self, obj):
        return format_html(
            '<a href="{}?customer={}">View invoices</a>',
            reverse('admin:crm_invoice_changelist'),
            obj.pk
        )
    view_invoices.short_description = 'Invoices'

    def changeform_view(self, request, object_id=None, form_url="", extra_context=None):
        extra_context = {}
        if object_id:
            customer = self.get_object(request, object_id)
            last_invoices = customer.invoice_set.order_by("-number")[:3]
            link_to_invoices = self.view_invoices(customer)

            today = date.today()
            last_year = date.today() - relativedelta(years=1)
            total_invoiced_in_the_last_year = customer.invoice_set.filter(invoiceDate__gt=last_year).aggregate(Sum('totalAmount'))['totalAmount__sum']


            extra_context = {
                "last_invoices": last_invoices,
                "total_invoiced_in_the_last_year": total_invoiced_in_the_last_year,
                "link_to_invoices": link_to_invoices,
            }
        return super(CustomerAdmin, self).changeform_view(request, object_id, form_url, extra_context)

class InvoiceAdmin(admin.ModelAdmin):
    list_display = [
        'customer',
        'invoiceNumber',
        'totalAmount',
        'invoiceDate',
        'paid',
    ]

    list_filter = ('customer__name',)

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(Customer, CustomerAdmin)
