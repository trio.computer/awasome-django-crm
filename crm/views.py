from django.shortcuts import render

from django.shortcuts import get_object_or_404
from .models import Customer, Invoice
from .serializers import InvoiceSerializer
from rest_framework import generics

class CustomerUnpaidInvoices(generics.ListAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer

    def get_queryset(self):
        customer = get_object_or_404(Customer, pk=self.kwargs.get('pk'))
        return self.queryset.filter(customer=customer).filter(paid=False)