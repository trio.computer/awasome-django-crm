from django.urls import path
from .views import CustomerUnpaidInvoices

urlpatterns = [
    path('api/customer/<int:pk>/invoices/unpaid', CustomerUnpaidInvoices.as_view(), name='customer-unpaid-invoices'),
]