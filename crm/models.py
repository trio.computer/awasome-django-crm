from django.db import models

class Customer(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    companyName = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100)
    invoicingCurrencyCode = models.CharField(max_length=3)

    def __str__(self):
        return self.name

class Invoice(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.RESTRICT)
    number = models.AutoField(primary_key=True)
    totalAmount = models.FloatField()
    invoiceDate = models.DateField()
    paid = models.BooleanField()

    @property
    def invoiceNumber(self):
        return 'OTA-' + str(self.number).zfill(5)